package сom.yurchyk.executor;

import сom.yurchyk.integers.RandomIntegers;
import сom.yurchyk.user.UserImplementation;

public class Executor {
    public static void startMyApp() {
        UserImplementation.userImplementation();
        RandomIntegers.randomIntegersImplementation();
    }
}