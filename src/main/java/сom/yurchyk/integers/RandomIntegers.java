package сom.yurchyk.integers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomIntegers {

    public static void randomIntegersImplementation() {
        List<Integer> randomIntegersList = new ArrayList<>();
        Random random = new Random();
        random.ints(15, 0, 100).boxed().forEach(randomIntegersList::add);
        System.out.println('\n' + "Second Task:" + '\n' + "List of random integers: " + randomIntegersList);

        findMaxValue(randomIntegersList);
        findMinValue(randomIntegersList);
        System.out.println('\n' + "List of values multiple by two: ");
        findValuesMultipleByTwo(randomIntegersList);
        System.out.println('\n' + "List of values increased by ten: ");
        numbersIncreaseByTen(randomIntegersList);
    }

    public static void findMaxValue(List<Integer> randomIntegersList) {
        int max = randomIntegersList.stream().reduce(Integer.MIN_VALUE, Integer::max);
        System.out.println("Max integer value: " + max);
    }

    public static void findMinValue(List<Integer> randomIntegersList) {
        int min = randomIntegersList.stream().reduce(Integer.MAX_VALUE, Integer::min);
        System.out.println("Min integer value: " + min);
    }

    public static void findValuesMultipleByTwo(List<Integer> randomIntegersList) {
        randomIntegersList.stream()
                .filter(integer -> integer % 2 == 0)
                .forEach(integer -> System.out.print(integer + " "));
    }

    public static void numbersIncreaseByTen(List<Integer> randomIntegersList) {
        randomIntegersList.stream()
                .mapToInt(value -> value + 10)
                .boxed()
                .forEach(integer -> System.out.print(integer + " "));
    }
}