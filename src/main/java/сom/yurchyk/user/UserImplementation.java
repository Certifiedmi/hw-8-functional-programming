package сom.yurchyk.user;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class UserImplementation {

    public static void userImplementation() {
        List<User> list = new ArrayList<>();
        list.add(new User("Danylo", 26, "Lviv"));
        list.add(new User("Roman", 23, "Kyiv"));
        list.add(new User("Kateryna", 20, "Kherson"));
        list.add(new User("Mykola", 24, "Dnipro"));
        list.add(new User("Tamara", 42, "Odesa"));
        list.add(new User("Vlad", 44, "Rivne"));

        System.out.println("First Task:" + '\n' + "List of users over 40: ");
        filterByAgeMoreThanForty(list);
        System.out.println('\n' + "List of users less than 50 from Dnipro: ");
        filterByAgeLessThanFiftyAndCityDnipro(list);
        System.out.print('\n' + "Average age of users from Lviv: ");
        getAverageAgeOfLvivUsers(list);
        System.out.print("Number of people who aren`t from Kyiv: ");
        filterByNotKyiv(list);
        System.out.print('\n' + "First 3 elements of list sorted by age: " + '\n');
        firstThreeElementsSortedByAge(list);
    }

    public static void filterByAgeMoreThanForty(List<User> list) {
        list.stream()
                .filter(user -> user.getAge() > 40)
                .forEach(System.out::println);
    }

    public static void filterByAgeLessThanFiftyAndCityDnipro(List<User> list) {
        list.stream()
                .filter(user -> user.getAge() < 50)
                .filter(user -> user.getCity().equals("Dnipro"))
                .forEach(System.out::println);
    }

    public static double filterByCityLviv(List<User> list) {
        return list.stream()
                .filter(user -> user.getCity().equals("Lviv"))
                .count();
    }

    private static double getSumOfAgeOfLvivUser(List<User> list) {
        return list.stream()
                .filter(user1 -> user1.getCity().equals("Lviv"))
                .mapToInt(User::getAge)
                .sum();
    }

    public static void getAverageAgeOfLvivUsers(List<User> list) {
        double averageAge = getSumOfAgeOfLvivUser(list) / filterByCityLviv(list);
        System.out.println(averageAge);
    }

    public static void filterByNotKyiv(List<User> list) {
        double notKyiv = list.stream()
                .filter(user -> !user.getCity().equals("Kyiv"))
                .count();
        System.out.println(notKyiv);
    }

    public static void firstThreeElementsSortedByAge(List<User> list) {
        list.stream()
                .sorted(Comparator.comparing(User::getAge))
                .limit(3)
                .forEach(System.out::println);
    }
}